package ua.com.pinta.caldroid;


import ua.com.pinta.caldroid.lib.roomorama.caldroid.CaldroidFragment;
import ua.com.pinta.caldroid.lib.roomorama.caldroid.CaldroidGridAdapter;

public class CaldroidSampleCustomFragment extends CaldroidFragment {

	@Override
	public CaldroidGridAdapter getNewDatesGridAdapter(int month, int year) {
		// TODO Auto-generated method stub
		return new CaldroidSampleCustomAdapter(getActivity(), month, year,
				getCaldroidData(), extraData);
	}

}
